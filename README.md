# Challenge Data 98
This is an ongoing report on my work on Challenge Data 98 "Detecting PIK3CA mutation in breast cancer" by Owkin.

## Setup and Preliminaries
The training data and supplementary files can be found on the challenge website [here](https://challengedata.ens.fr/participants/challenges/98/).
We assume that the directory has the following structure, as described in the baseline notebook `supplementary_data/baseline.ipynb`:
```
your_data_dir/
├── train_output.csv
├── train_input/
│   ├── images/
│       ├── ID_001/
│           ├── ID_001_tile_000_17_170_43.jpg
...
│   └── moco_features/
│       ├── ID_001.npy
...
├── test_input/
│   ├── images/
│       ├── ID_003/
│           ├── ID_003_tile_000_16_114_93.jpg
...
│   └── moco_features/
│       ├── ID_003.npy
...
├── supplementary_data/
│   ├── baseline.ipynb
│   ├── test_metadata.csv
│   └── train_metadata.csv
```

### Dependencies
Besides NumPy and Pandas, the primary machine learning package we use is `fastai`.
Installation instructions for fastai can be found in its [docs](https://docs.fast.ai/).

## Notebooks
`CHOWDER.ipynb`: In this notebook we attempt to implement variations of the CHOWDER method as defined in the paper "Classification And Disease Localization In Histopathology Using Only Global Labels: A Weakly-supervised Approach", published by researchers at Owkin.
This can be found in the arxiv at https://arxiv.org/pdf/1802.02212.pdf.
